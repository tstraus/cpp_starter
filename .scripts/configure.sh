#!/bin/bash

BUILD_TYPE='Debug'

if [[ $1 == '-r' || $1 == '--release' ]]; then
    BUILD_TYPE='Release'
fi

if [ ! -d build ]; then
    mkdir build
fi

cd build

if [[ -e `command -v clang` && -e `command -v clang++` ]]; then
    export CC=`command -v clang`
    export CXX=`command -v clang++`

    if [[ $BUILD_TYPE == 'Release' && -e `command -v lld` ]]; then # manual lld and LTO
        LLD_FLAGS='-DCMAKE_CXX_FLAGS=-flto -DCMAKE_C_FLAGS=-flto -DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=lld -DCMAKE_SHARED_LINKER_FLAGS=-fuse-ld=lld'
    fi
fi

if [[ -e `command -v vcpkg` ]]; then
    UNAME=$(uname -s)
    if [[ "${UNAME:0:5}" == "MINGW" ]]; then
        TOOLCHAIN_FLAGS="-DVCPKG_TARGET_TRIPLET=x64-windows-static -DVCPKG_HOST_TRIPLET=x64-windows-static"
    fi

    TOOLCHAIN_FLAGS="${TOOLCHAIN_FLAGS} -DCMAKE_TOOLCHAIN_FILE=$(dirname `command -v vcpkg`)/scripts/buildsystems/vcpkg.cmake"
fi

if [[ -e `command -v ninja` || -e `command -v ninja-build` ]]; then
    cmake -G Ninja $TOOLCHAIN_FLAGS $LLD_FLAGS -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX:PATH=`pwd`/dist ..
else
    cmake $TOOLCHAIN_FLAGS $LLD_FLAGS -DCMAKE_BUILD_TYPE=$BUILD_TYPE -DCMAKE_INSTALL_PREFIX:PATH=`pwd`/dist ..
fi
