find_package(CLI11 REQUIRED)
find_package(spdlog REQUIRED)
find_package(Crow REQUIRED)
find_package(flatbuffers REQUIRED)
find_package(ZeroMQ REQUIRED)
find_package(GTest)
find_package(benchmark)

find_package(Qt6 COMPONENTS Widgets)
if(${Qt6_FOUND})
    # let CMake figure out the ui stuff
    set(CMAKE_AUTOMOC ON)
    set(CMAKE_AUTORCC ON)
    set(CMAKE_AUTOUIC ON)
endif()

# dont try if too new of a compiler for cuda
#set(CAN_TORCH TRUE)
#if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
#    if(${CMAKE_CXX_COMPILER_VERSION} VERSION_GREATER_EQUAL "13")
#        set(CAN_TORCH FALSE)
#    endif()
#elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
#    if(${CMAKE_CXX_COMPILER_VERSION} VERSION_GREATER_EQUAL "16")
#        set(CAN_TORCH FALSE)
#    endif()
#endif()
#
#if(${CAN_TORCH})
#    find_package(Torch)
#endif()

