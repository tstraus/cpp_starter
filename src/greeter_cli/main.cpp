#include "greeter.hpp"

#include "CLI/CLI.hpp"

int main(int argc, char** argv)
{
    std::string name = "Margot";

    CLI::App app{"I'll say hello to anyone you want"};
    app.add_option("-n,--name", name, "Who to say hello to");
    CLI11_PARSE(app, argc, argv);

    greeter::Greeter greeter;
    greeter.greet(name);

    return 0;
}
