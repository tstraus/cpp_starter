#include "greeter.hpp"

#include "crow.h"

int main(int argc, char** argv)
{
    greeter::Greeter g;

    crow::SimpleApp app;

    CROW_ROUTE(app, "/<string>")
    ([&](std::string_view name) {
        g.greet(name);
        return "";
    });

    app.bindaddr("127.0.0.1").port(1234).run();

    return 0;
}
