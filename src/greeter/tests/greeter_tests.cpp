#include "greeter.hpp"
#include "person_fbs/person_generated.h"

#include "gtest/gtest.h"

using greeter::Greeter;

#define TEST_NAME "Margot"

TEST(GreeterTests, StrGreet)
{
    Greeter g;

    ASSERT_NO_THROW(g.greet(TEST_NAME));
}

TEST(GreeterTests, StrLength)
{
    Greeter g;

    const auto length = g.greet(TEST_NAME);
    EXPECT_EQ(length, 6);
}

TEST(GreeterTests, StrCount)
{
    Greeter g;

    ASSERT_EQ(g.totalGreeted(), 0);

    g.greet("one");
    ASSERT_EQ(g.totalGreeted(), 1);

    g.greet("two");
    ASSERT_EQ(g.totalGreeted(), 2);

    g.greet("three");
    ASSERT_EQ(g.totalGreeted(), 3);
}

TEST(GreeterTests, BufGreet)
{
    Greeter g;

    greeter::PersonT in;
    in.id = 12893;
    in.name = TEST_NAME;
    in.email = "asdf@gmail.com";

    flatbuffers::FlatBufferBuilder fbb;
    fbb.Finish(greeter::Person::Pack(fbb, &in));

    ASSERT_NO_THROW(g.greet_buf(fbb.GetBufferSpan()));
}

TEST(GreeterTests, BufLength)
{
    Greeter g;

    greeter::PersonT in;
    in.id = 12893;
    in.name = TEST_NAME;
    in.email = "asdf@gmail.com";

    flatbuffers::FlatBufferBuilder fbb;
    fbb.Finish(greeter::Person::Pack(fbb, &in));

    const auto length = g.greet_buf(fbb.GetBufferSpan());
    EXPECT_EQ(length, 6);
}

