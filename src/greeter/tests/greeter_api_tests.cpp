#include "greeter_api.h"
#include "person_fbs/person_generated.h"

#include "gtest/gtest.h"

#define TEST_NAME "Margot"

TEST(GreeterApiTests, Greet)
{
    auto* g = greeter_new();
    ASSERT_NE(g, nullptr);

    EXPECT_GT(greeter_greet(g, TEST_NAME), 0);

    auto* gc = g;
    greeter_free(g);
    ASSERT_EQ(g, gc);
}

TEST(GreeterApiTests, Length)
{
    auto* g = greeter_new();

    const auto length = greeter_greet(g, TEST_NAME);
    EXPECT_EQ(length, 6);

    greeter_free(g);
}

TEST(GreeterApiTests, Count)
{
    auto* g = greeter_new();

    ASSERT_EQ(greeter_total_greeted(g), 0);

    greeter_greet(g, "one");
    ASSERT_EQ(greeter_total_greeted(g), 1);

    greeter_greet(g, "two");
    ASSERT_EQ(greeter_total_greeted(g), 2);

    greeter_greet(g, "three");
    ASSERT_EQ(greeter_total_greeted(g), 3);
}

TEST(GreeterApiTests, BufGreet)
{
    auto* g = greeter_new();
    ASSERT_NE(g, nullptr);

    greeter::PersonT in;
    in.id = 12893;
    in.name = TEST_NAME;
    in.email = "asdf@gmail.com";

    flatbuffers::FlatBufferBuilder fbb;
    fbb.Finish(greeter::Person::Pack(fbb, &in));

    EXPECT_GT(greeter_greet_buf(g, fbb.GetBufferPointer(), fbb.GetSize()), 0);

    auto* gc = g;
    greeter_free(g);
    ASSERT_EQ(g, gc);
}

TEST(GreeterApiTests, BufLength)
{
    auto* g = greeter_new();
    ASSERT_NE(g, nullptr);

    greeter::PersonT in;
    in.id = 12893;
    in.name = TEST_NAME;
    in.email = "asdf@gmail.com";

    flatbuffers::FlatBufferBuilder fbb;
    fbb.Finish(greeter::Person::Pack(fbb, &in));

    const auto length = greeter_greet_buf(g, fbb.GetBufferPointer(), fbb.GetSize());
    EXPECT_EQ(length, 6);

    greeter_free(g);
}

