#pragma once

#include "flatbuffers/flatbuffers.h"
#include "zmq.h"

#include <functional>
#include <iostream>
#include <string_view>
#include <thread>

namespace io
{
template <typename T>
class Subscriber
{
  public:
    Subscriber(std::function<void(const T&)> callback, std::string_view url = "udp://127.255.255.255:1234")
        : cb(std::move(callback))
    {
        context = zmq_ctx_new();
        dish = zmq_socket(context, ZMQ_DISH);

        zmq_bind(dish, url.data());
        zmq_join(dish, T::GetFullyQualifiedName());

        recv_thread = std::thread([&]() {
            while (!stop)
            {
                zmq_msg_t msg;
                zmq_msg_init(&msg);

                const auto rc = zmq_msg_recv(&msg, dish, 0);

                if (rc > 0)
                {
                    const auto* const out = flatbuffers::GetRoot<T>(zmq_msg_data(&msg));
                    cb(*out);
                }

                zmq_msg_close(&msg);
            }
        });
    }

    ~Subscriber()
    {
        stop = true;
        zmq_close(dish);
        zmq_ctx_destroy(context);

        recv_thread.join();
    }

  private:
    void* context;
    void* dish;

    bool stop = false;

    std::thread recv_thread;
    std::function<void(const T&)> cb;
};
} // namespace io
