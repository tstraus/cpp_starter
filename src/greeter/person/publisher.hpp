#pragma once

#include "zmq.h"
#include "flatbuffers/flatbuffers.h"

#include <string_view>

namespace io
{
template<typename T>
class Publisher
{
  public:
    Publisher(std::string_view url = "udp://127.255.255.255:1234")
    {
        context = zmq_ctx_new();
        radio = zmq_socket(context, ZMQ_RADIO);

        zmq_connect(radio, url.data());
    }

    ~Publisher()
    {
        zmq_close(radio);
        zmq_ctx_destroy(context);
    }

    void send(const typename T::NativeTableType& in)
    {
        flatbuffers::FlatBufferBuilder fbb;
        fbb.Finish(T::Pack(fbb, &in));
        const auto msg_size = fbb.GetSize();

        size_t size = 0;
        size_t offset = 0;
        const auto* const raw = fbb.ReleaseRaw(size, offset);

        zmq_msg_t msg;
        zmq_msg_init_data(&msg, (void*)(raw + offset), msg_size, free_message, (void*)raw);
        zmq_msg_set_group(&msg, T::GetFullyQualifiedName());
        zmq_msg_send(&msg, radio, 0);
    }

  private:
    static void free_message(void* data, void* hint)
    {
        (void)data;
        flatbuffers::DefaultAllocator().deallocate((uint8_t*)hint, 0);
    }

    void* context;
    void* radio;
};
} // namespace io
