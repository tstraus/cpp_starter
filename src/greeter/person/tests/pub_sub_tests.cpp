#include "gtest/gtest.h"

#include "publisher.hpp"
#include "subscriber.hpp"

#include "person_fbs/person_generated.h"

#include <atomic>

using namespace greeter;
using namespace flatbuffers;

std::string url = "udp://127.0.0.1:1234";

TEST(PubSub, Basic)
{
    PersonT out;

    PersonT in;
    in.id = 12893;
    in.name = "asdf";
    in.email = "asdf@gmail.com";

    {
        bool received = false;

        io::Subscriber<Person> sub(
            [&](const auto& p) {
                p.UnPackTo(&out);
                received = true;

                std::cout << "received: " << p.email()->string_view() << std::endl;
            },
            url);

        io::Publisher<Person> pub(url);

        pub.send(in);

        while (!received) // NOLINT
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        }
    }

    EXPECT_EQ(in.id, out.id);
    EXPECT_EQ(in.name, out.name);
    EXPECT_EQ(in.email, out.email);
}

// TEST(PubSub, DISABLED_Bench)
//{
//    PersonT out;
//
//    PersonT in;
//    in.id = 12893;
//    in.name = "asdf";
//    in.email = "asdf@gmail.com";
//
//    {
//        std::atomic<uint32_t> count(0);
//        io::PersonSubscriber sub(
//            [&](const auto& p) {
//                p.UnPackTo(&out);
//                count++;
//            },
//            url);
//
//        io::PersonPublisher pub(url);
//
//        for (int i = 0; i < 1000; i++)
//        {
//            pub.send(in);
//        }
//
//        while (count.load(std::memory_order_acquire) != 1000) // NOLINT
//        {
//            ;
//        }
//    }
//}
