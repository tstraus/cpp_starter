#include "person_fbs/person_generated.h"

#include "flatbuffers/minireflect.h"

#include "gtest/gtest.h"

using namespace greeter;
using namespace flatbuffers;

TEST(Person, DefaultApi)
{
    FlatBufferBuilder fbb;

    uint32_t id = 12893;
    std::string name = "asdf fdsa";
    std::string email = "asdf.fdsa@gmail.com";

    std::array<PhoneNumber, 2> pn;
    pn[0] = {13272677976, PhoneType::Mobile};
    pn[1] = {13037670468, PhoneType::Work};

    const auto fb_name = fbb.CreateString(name);
    const auto fb_email = fbb.CreateString(email);
    const auto fb_phones = fbb.CreateVectorOfStructs(pn.data(), pn.size());

    PersonBuilder pb(fbb);
    pb.add_id(id);
    pb.add_name(fb_name);
    pb.add_email(fb_email);
    pb.add_phones(fb_phones);
    fbb.Finish(pb.Finish());

    // fbb.Finish(CreatePerson(fbb, id, fbb.CreateString(name), fbb.CreateString(email)));

    std::cout << "size: " << fbb.GetBufferSpan().size_bytes() << std::endl;

    const auto* const out = GetRoot<Person>(fbb.GetBufferPointer());

    EXPECT_EQ(id, out->id());
    EXPECT_EQ(name, out->name()->string_view());
    EXPECT_EQ(email, out->email()->string_view());
    EXPECT_EQ(pn.size(), out->phones()->size());
    EXPECT_EQ(pn[0], *out->phones()->Get(0));
    EXPECT_EQ(pn[1], *out->phones()->Get(1));
}

TEST(Person, ObjectApi)
{
    PersonT in;
    in.id = 12893;
    in.name = "asdf";
    in.email = "asdf@gmail.com";

    FlatBufferBuilder fbb;
    fbb.Finish(Person::Pack(fbb, &in));

    std::cout << "size: " << fbb.GetBufferSpan().size_bytes() << std::endl;

    const auto* const out_buff = GetRoot<Person>(fbb.GetBufferPointer());
    EXPECT_EQ(in.id, out_buff->id());
    EXPECT_EQ(in.name, out_buff->name()->string_view());
    EXPECT_EQ(in.email, out_buff->email()->string_view());

    PersonT out;
    out_buff->UnPackTo(&out);

    EXPECT_EQ(in.id, out.id);
    EXPECT_EQ(in.name, out.name);
    EXPECT_EQ(in.email, out.email);
}

TEST(Person, ToJson)
{
    PersonT in;
    in.id = 12893;
    in.name = "asdf";
    in.email = "asdf@gmail.com";

    FlatBufferBuilder fbb;
    fbb.Finish(Person::Pack(fbb, &in));

    const auto s = flatbuffers::FlatBufferToString(fbb.GetBufferPointer(), PersonTypeTable());

    std::cout << s << std::endl;

    EXPECT_EQ(s, R"({ id: 12893, name: "asdf", email: "asdf@gmail.com" })");
}

