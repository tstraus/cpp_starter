#include "greeter_api.h"
#include "greeter.hpp"

void* greeter_new()
{
    return reinterpret_cast<void*>(new greeter::Greeter());
}

void greeter_free(void* context)
{
    if (context != nullptr)
    {
        delete reinterpret_cast<greeter::Greeter*>(context);
        context = nullptr;
    }
}

size_t greeter_greet(void* context, const char* name)
{
    return reinterpret_cast<greeter::Greeter*>(context)->greet(name);
}

size_t greeter_greet_buf(void* context, const uint8_t* buf_data, size_t buf_size)
{
    return reinterpret_cast<greeter::Greeter*>(context)->greet_buf({buf_data, buf_size});
}

size_t greeter_total_greeted(void* context)
{
    return reinterpret_cast<greeter::Greeter*>(context)->totalGreeted();
}
