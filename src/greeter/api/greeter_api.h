#ifndef GREETER_API_H
#define GREETER_API_H

#include <stddef.h> // NOLINT
#include <stdint.h> // NOLINT

#if defined(_WIN32)
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __attribute__((visibility("default")))
#endif

#ifdef __cplusplus
extern "C"
{
#endif
    EXPORT void* greeter_new();

    EXPORT void greeter_free(void* context);

    EXPORT size_t greeter_greet(void* context, const char* name);

    EXPORT size_t greeter_greet_buf(void* context, const uint8_t* buf_data, size_t buf_size);

    EXPORT size_t greeter_total_greeted(void* context);
#ifdef __cplusplus
}
#endif

#endif // GREETER_API_H
