#pragma once

#include <string_view>
#include <span>
#include <shared_mutex>

namespace greeter
{
class Greeter
{
  public:
    Greeter();
    ~Greeter();

    size_t greet(std::string_view name);
    size_t greet_buf(std::span<const uint8_t> person_buf);

    size_t totalGreeted() const;

  private:
    mutable std::shared_mutex totalMutex;
    size_t total;
};
} // namespace greeter

