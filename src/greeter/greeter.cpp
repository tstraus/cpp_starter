#include "greeter.hpp"

#include "person_fbs/person_generated.h"

#include "spdlog/spdlog.h"

namespace greeter
{
Greeter::Greeter() : total(0)
{
}

Greeter::~Greeter() = default;

size_t Greeter::greet(std::string_view name)
{
    spdlog::info("Hola {}!", name.data());

    {
        std::unique_lock<std::shared_mutex> lock(totalMutex);
        total++;
    }

    return name.length();
}

size_t Greeter::greet_buf(std::span<const uint8_t> person_buf)
{
    const auto* const person = flatbuffers::GetRoot<greeter::Person>(person_buf.data());

    spdlog::debug("email: {}", person->email()->string_view());

    return greet(person->name()->string_view());
}

size_t Greeter::totalGreeted() const
{
    size_t temp;

    {
        std::shared_lock<std::shared_mutex> lock(totalMutex);
        temp = total;
    }

    return temp;
}
} // namespace greeter
