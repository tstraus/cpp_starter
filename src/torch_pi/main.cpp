#include "CLI/CLI.hpp"

#include "torch/torch.h"

int main(int argc, char** argv)
{
    int loops = 100;
    int batch = 100000000;
    bool force_cpu = false;

    CLI::App app{"Monte Carlo Estimate of PI using libtorch"};
    app.add_option("-l,--loops", loops, std::string("Outer loop size, default: ") + std::to_string(loops));
    app.add_option("-b,--batch", batch, std::string("Inner loop size, default: ") + std::to_string(batch));
    app.add_flag("-c,--cpu", force_cpu, "Force libtorch to use CPU as device");
    CLI11_PARSE(app, argc, argv);

    const auto total = (uint64_t)loops * (uint64_t)batch;

    const auto device = torch::Device([&]() {
        if (force_cpu)
        {
            return torch::kCPU;
        }
        if (torch::cuda::is_available())
        {
            return torch::kCUDA;
        }

        if (torch::mps::is_available())
        {
            return torch::kMPS;
        }

        return torch::kCPU;
    }());

    std::cout << "device: " << device << std::endl;
    std::cout << "samples: " << total << std::endl;

    // const auto t = torch::eye(3, device);
    // std::cout << t << std::endl;

    auto in_counts = torch::zeros(loops, device);

    for (int i = 0; i < loops; i++)
    {
        auto x = torch::rand(batch, device); // NOLINT
        auto y = torch::rand(batch, device); // NOLINT

        x = x * x;
        y = y * y;
        x = x + y;
        y = torch::ones(batch, device);

        in_counts[i] = (x < y).count_nonzero();
    }

    const auto count = torch::sum(in_counts).item().to<double>();
    const auto pi = count / (total / 4.0); // NOLINT

    std::cout << "pi: " << std::setprecision(10) << pi << std::endl;

    return 0;
}
