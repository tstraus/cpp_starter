# builder container
FROM debian:stable-slim AS builder

# get vcpkg
RUN apt update && apt -y install build-essential cmake git pkg-config ninja-build curl unzip tar zip dos2unix
RUN git clone https://github.com/Microsoft/vcpkg.git /opt/vcpkg
WORKDIR /opt/vcpkg
RUN ./bootstrap-vcpkg.sh && ./vcpkg integrate install && ./vcpkg integrate bash && echo 'export PATH=$PATH:/opt/vcpkg' >>~/.bashrc
WORKDIR /app

# build the project
COPY README.md ./
COPY vcpkg.json ./
COPY CMakeLists.txt ./
COPY .scripts ./scripts
COPY src ./src
RUN dos2unix scripts/*.sh
RUN export PATH=$PATH:/opt/vcpkg && scripts/configure.sh -r
RUN scripts/build.sh -r
RUN scripts/test.sh -r

# runner container
FROM debian:stable-slim
WORKDIR /app
COPY --from=0 /app/build/dist ./

EXPOSE 1234

ENTRYPOINT ["/app/bin/greeter_server"]
